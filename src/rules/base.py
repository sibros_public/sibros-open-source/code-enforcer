from abc import ABC, abstractmethod
from copy import deepcopy
import os

from clang.cindex import CursorKind


class ViolationError(Exception):
    pass


class LanguageRestriction:
    ANY = "any"
    C = "c"
    CPP = "cpp"


class RuleABC(ABC):
    COLUMNS = ["Rule", "Brief", "Description", "Cursor kinds", "Error"]

    def __init__(
        self,
        name=None,
        brief=None,
        description=None,
        cursor_kinds=None,
        exceptions=[],
        language_restrictions=LanguageRestriction.ANY,
    ):
        """
        :param name: Name of the rule (str)
        :param brief: Short description of the rule (str)
        :param description: Description of the rule (str)
        :param cursor_kinds: A list of cursor kinds the rule shall be applicable (i.e. CursorKind.VAR_DECL)
        For a full list of cursor kinds, see: https://github.com/llvm-mirror/clang/blob/master/bindings/python/clang/cindex.py
        :param language_restrictions: A list of restricted languages (LanguageRestriction.<CONSTANT>)
        """
        self._name = name
        self._brief = brief
        self._description = description
        self._exceptions = exceptions

        if isinstance(cursor_kinds, CursorKind):
            self._cursor_kinds = [cursor_kinds]
        elif isinstance(cursor_kinds, list):
            self._cursor_kinds = cursor_kinds
        else:
            raise AssertionError("Invalid cursor kinds input! Got [{}]".format(cursor_kinds))

        if isinstance(language_restrictions, str):
            language_restrictions = [language_restrictions]
        self._language_restrictions = language_restrictions

        str(
            self._cursor_kinds
        )  # HACK; convert to string to initialize CursorKind __repr__ global state

    def __iter__(self):
        for attr in [
            self.name,
            self.brief,
            self.description,
            ", ".join(list(map(lambda cursor_kind: cursor_kind.name, self.cursor_kinds))),
            self.error,
        ]:
            yield attr

    """
    Public methods
    """

    def enforce(self, cursor):
        if self.is_applicable(cursor) and not self.is_excepted(cursor):
            return self._enforce(cursor)

    def is_applicable(self, cursor):
        """
        Check if this rule is applicable for this cursor
        :param cursor: Clang cursor (Cursor)
        :return: True if rule is applicable
        """
        return (cursor.kind in self._cursor_kinds) and self._is_language_applicable(cursor)

    def is_excepted(self, cursor):
        for exception in self._exceptions:
            if exception(cursor):
                return True
        return False

    """
    Private methods
    """

    def _is_language_applicable(self, cursor):
        """Check if rule is applicable to the language of the file"""
        if (
            LanguageRestriction.ANY in self._language_restrictions
        ):  # Check if there are no language restrictions
            return True

        basename, ext = os.path.splitext(cursor.location.file.name)
        applicable = True
        if ext in [".c", ".h"]:
            if LanguageRestriction.C not in self._language_restrictions:
                applicable = False
        else:  # Assume CPP
            if LanguageRestriction.CPP not in self._language_restrictions:
                applicable = False

        return applicable

    """
    Private methods - abstract
    """

    @abstractmethod
    def _enforce(self, cursor):
        """
        This method shall, given a cursor, implement a rule to enforce on the cursor.
        If a violation is detected, this method shall raise a ViolationError exception.
        Example: A variable name shall start with a letter.
            if cursor.spelling.startswith("_"):
                raise ViolationError
        :param cursor: Clang cursor (Cursor)
        :raise: ViolationError
        """
        raise NotImplementedError

    """
    Accessors
    """

    @property
    def name(self):
        return self._name

    @property
    def brief(self):
        return self._brief

    @property
    def description(self):
        return self._description

    @property
    def cursor_kinds(self):
        return deepcopy(self._cursor_kinds)

    """
    Accessors - abstract
    """

    @property
    def error(self):
        return ""
