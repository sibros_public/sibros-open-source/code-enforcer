import re

from .base import RuleABC, ViolationError


class RuleRegex(RuleABC):
    """
    Symbol shall match regular expression
    """

    def __init__(self, regex_generator, **kwargs):
        RuleABC.__init__(self, **kwargs)
        self._regex_generator = regex_generator

    """
    Private methods
    """

    def _enforce(self, cursor):
        if not re.match(self._regex_generator[self.name](cursor), cursor.spelling):
            raise ViolationError
