import inspect

from clang.cindex import CursorKind

from rules import *
import sibros_exceptions
from sibros_regex_patterns import regex_dict


all_rules = [
    RuleRegex(
        name="general_rule_1",
        brief="Lower case snake case naming convention",
        description=inspect.cleandoc(
            """\
        All variable names, function names, struct/union/enum tags, and typedef names shall use lower_case_snake_case, except:
        - Macros
        - Enumeration values
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[
            CursorKind.VAR_DECL,
            CursorKind.FUNCTION_DECL,
            CursorKind.CXX_METHOD,
            CursorKind.PARM_DECL,
            CursorKind.FIELD_DECL,
            CursorKind.STRUCT_DECL,
            CursorKind.TYPEDEF_DECL,
            CursorKind.ENUM_DECL,
            CursorKind.UNION_DECL,
            CursorKind.CLASS_DECL,
        ],
        exceptions=[sibros_exceptions.except_well_known_macros, sibros_exceptions.except_extern],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="general_rule_2",
        brief="Upper case snake case naming convention",
        description=inspect.cleandoc(
            """\
        All macros shall use UPPER_CASE_SNAKE_CASE
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.MACRO_DEFINITION],
        exceptions=[sibros_exceptions.except_well_known_macros, sibros_exceptions.except_extern],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="general_rule_3",
        brief="General typedef naming convention",
        description=inspect.cleandoc(
            """\
        Typedef symbols that are not structs/unions/enums/function pointers shall end with the suffix _t. (e.g. my_type_t)
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.TYPEDEF_DECL],
        exceptions=[
            sibros_exceptions.except_non_general_typedefs  # This is a required exception because it only makes sense for this rule to run on general typedefs
        ],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="general_rule_4",
        brief="Layer access rule",
        description=inspect.cleandoc(
            """\
        In embedded/c_code, files in certain layers shall not access to types/methods/data from restricted layers.
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.INCLUSION_DIRECTIVE],
        exceptions=[],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="general_rule_5",
        brief="Global variable name format",
        description=inspect.cleandoc(
            """\
        Global-scope variables shall be named in the format <file name>__<purpose>
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.VAR_DECL],
        exceptions=[
            sibros_exceptions.except_local_cursor,
            sibros_exceptions.except_header_guard_macro,
            sibros_exceptions.except_well_known_macros,
            sibros_exceptions.except_extern,
        ],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="variable_rule_1",
        brief="No non-const local static variable declarations",
        description=inspect.cleandoc(
            """\
        Non-const static variables shall not be declared in functions.
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.VAR_DECL],
        exceptions=[
            sibros_exceptions.except_const_variable,
            sibros_exceptions.except_global_variable,
            sibros_exceptions.except_non_static_variable,
        ],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="macro_rule_1",
        brief="Header guard matching file name rule",
        description=inspect.cleandoc(
            """\
        All header guards shall be in the format <upper case file name>_H
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.MACRO_DEFINITION],
        exceptions=[],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="struct_rule_1",
        brief="Structure tag name suffix",
        description=inspect.cleandoc(
            """\
        Struct tags shall end with the suffix _tag. (e.g. my_struct_name_tag)
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.STRUCT_DECL],
        exceptions=[sibros_exceptions.except_well_known_macros, sibros_exceptions.except_extern],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="struct_rule_2",
        brief="Typedef structure name suffix",
        description=inspect.cleandoc(
            """\
        Typedef struct names shall end with the suffix _s. (e.g. my_struct_name_s)
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.TYPEDEF_DECL],
        exceptions=[
            sibros_exceptions.except_non_struct_typedefs  # This is a required exception because it only makes sense for this rule to run on struct typedefs
        ],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="struct_rule_3",
        brief="Typedef structure name format",
        description=inspect.cleandoc(
            """\
        Typedef struct names shall be named in the format <file name>__<purpose>_s or <file name>_s (e.g. app_can_rx_mgr__recipient_s, app_can_logger_s)
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.TYPEDEF_DECL],
        exceptions=[
            sibros_exceptions.except_non_struct_typedefs  # This is a required exception because it only makes sense for this rule to run on struct typedefs
        ],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="struct_rule_4",
        brief="Typedef struct name and struct tag match",
        description=inspect.cleandoc(
            """\
        Struct tag name must match typedef struct name (e.g. sl_code_header_tag AND sl_code_header_s)
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.TYPEDEF_DECL],
        exceptions=[
            sibros_exceptions.except_non_struct_typedefs  # This is a required exception because it only makes sense for this rule to run on struct typedefs
        ],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="union_rule_1",
        brief="Union tag name suffix",
        description=inspect.cleandoc(
            """\
        Union tags shall end with the suffix _tag. (e.g. my_struct_name_tag)
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.UNION_DECL],
        exceptions=[sibros_exceptions.except_well_known_macros, sibros_exceptions.except_extern],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="union_rule_2",
        brief="Typedef union name suffix",
        description=inspect.cleandoc(
            """\
        Typedef union names shall end with the suffix _u. (e.g. my_union_name_u)
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.TYPEDEF_DECL],
        exceptions=[
            sibros_exceptions.except_non_union_typedefs  # This is a required exception because it only makes sense for this rule to run on union typedefs
        ],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="union_rule_3",
        brief="Typedef union name format",
        description=inspect.cleandoc(
            """\
        Typedef union names shall be named in the format <file name>__<purpose>_u (e.g. app_can_logger__header_u)
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.TYPEDEF_DECL],
        exceptions=[
            sibros_exceptions.except_non_union_typedefs  # This is a required exception because it only makes sense for this rule to run on union typedefs
        ],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="union_rule_4",
        brief="Typedef union name and union tag match",
        description=inspect.cleandoc(
            """\
        Union tag name must match typedef union name (e.g. my_union_name__purpose_tag AND my_union_name__purpose_s)
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.TYPEDEF_DECL],
        exceptions=[
            sibros_exceptions.except_non_union_typedefs  # This is a required exception because it only makes sense for this rule to run on union typedefs
        ],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="enum_rule_1",
        brief="Enum tag name suffix",
        description=inspect.cleandoc(
            """\
        Enum tags shall end with the suffix _tag. (e.g. my_enum_name_tag)
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.ENUM_DECL],
        exceptions=[sibros_exceptions.except_well_known_macros, sibros_exceptions.except_extern],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="enum_rule_2",
        brief="Typedef enum name suffix",
        description=inspect.cleandoc(
            """\
        Typedef enum names shall end with the suffix _e. (e.g. my_enum_name_e)
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.TYPEDEF_DECL],
        exceptions=[
            sibros_exceptions.except_non_enum_typedefs  # This is a required exception because it only makes sense for this rule to run on enum typedefs
        ],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="enum_rule_3",
        brief="Typedef enum name format",
        description=inspect.cleandoc(
            """\
        Typedef enum names shall be named in the format <file name>__<purpose>_e (e.g. app_can_logger__version_e)
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.TYPEDEF_DECL],
        exceptions=[
            sibros_exceptions.except_non_enum_typedefs  # This is a required exception because it only makes sense for this rule to run on enum typedefs
        ],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="enum_rule_4",
        brief="Typedef enum name and enum tag match",
        description=inspect.cleandoc(
            """\
        Enum tag name must match typedef enum name (e.g. app_can_logger__version_tag AND app_can_logger__version_e)
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.TYPEDEF_DECL],
        exceptions=[
            sibros_exceptions.except_non_enum_typedefs  # This is a required exception because it only makes sense for this rule to run on enum typedefs
        ],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="enum_rule_5",
        brief="Enum member name format",
        description=inspect.cleandoc(
            """\
        Enum members shall be named after their enum tag name in the format <enum base name>_<purpose> in UPPER_CASE_SNAKE_CASE
        (e.g. app_can_logger__version_tag HAS APP_CAN_LOGGER__VERSION_WITHOUT_MICROSECOND_TIMESTAMP,
        app_http_download__state_tag HAS APP_HTTP_DOWNLOAD__STATE_NOT_STARTED), except:
        - Enums with _private in name must have its members named without _private
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.ENUM_CONSTANT_DECL],
        exceptions=[sibros_exceptions.except_well_known_macros, sibros_exceptions.except_extern],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="function_rule_1",
        brief="Non-private function name format",
        description=inspect.cleandoc(
            """\
        Non-static functions shall be named in the format <file name>__<purpose> (e.g. sl_can__create_message())
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.FUNCTION_DECL],
        exceptions=[
            sibros_exceptions.except_extern,
            sibros_exceptions.is_function_static,
            sibros_exceptions.is_function_main,
        ],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="function_rule_2",
        brief="Private function name format",
        description=inspect.cleandoc(
            """\
        Static functions shall be named in the format <file name>__private_<purpose> (e.g. dev_http_client__private_send_request())
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.FUNCTION_DECL],
        exceptions=[sibros_exceptions.except_extern, sibros_exceptions.is_function_non_static],
        language_restrictions=LanguageRestriction.C,
    ),
    RuleRegex(
        name="function_rule_3",
        brief="Typedef function pointer name suffix",
        description=inspect.cleandoc(
            """\
        Typedef function pointer names shall end with the suffix _f. (e.g. my_function_pointer_name_f)
        """
        ),
        regex_generator=regex_dict,
        cursor_kinds=[CursorKind.TYPEDEF_DECL],
        exceptions=[
            sibros_exceptions.except_non_function_pointer_typedefs  # This is a required exception because it only makes sense for this rule to run on function pointer typedefs
        ],
        language_restrictions=LanguageRestriction.C,
    ),
]
