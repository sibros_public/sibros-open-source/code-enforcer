import os
import re
import sys
import string


def regex_generator_general_rule_1(cursor):
    return "^(?:[a-z0-9]+_+)*[a-z0-9]+$"


def regex_generator_general_rule_2(cursor):
    return "^(?:[A-Z0-9]+_+)*[A-Z0-9]+$"


def regex_generator_general_rule_3(cursor):
    return "^.*_[t]{1}$"


def regex_generator_general_rule_4(cursor):
    """ Regex pattern to ensure that included file does not come from a restricted layer """
    allowed_layer_access_dict = {
        # Refers to table in https://gitlab.com/sibros_public/public/-/wikis/c/coding_standards
        # layer : not allowed access regex list
        "app_": ["^perif_"],
        "dev_": ["^app_", "^perif_"],
        "ecu_": ["^app_", "^dev_", "^perif_"],
        "mcu_": ["^app_", "^dev_", "^ecu_", "^os_"],
        "os_": ["^app_", "^dev_", "^ecu_", "^perif_"],
        "perif_": ["^app_", "^dev_", "^ecu_", "^mcu_", "^os_"],
        "sl_": ["^app_", "^dev_", "^ecu_", "^mcu_", "^os_", "^perif_"],
    }

    # Rule only applies to code in c_code and Code Enforcer test code
    directories_to_enforce = ["embedded/c_code", "embedded/tools/code_enforcer/test/test_code"]

    enforce_cursor = False
    for directory_to_enforce in directories_to_enforce:
        if directory_to_enforce in cursor.location.file.name:
            enforce_cursor = True
    if (
        "_config.c" in cursor.location.file.name
    ):  # Files ending in _config.c contain callbacks, so are allowed to access layers above them
        enforce_cursor = False

    returned_regex = "."  # Regex to match anything

    if not enforce_cursor:
        return returned_regex

    for layer in allowed_layer_access_dict:
        if re.match(layer, os.path.basename(cursor.location.file.name)):
            for acceptable_include_regex in allowed_layer_access_dict[layer]:
                if re.match(acceptable_include_regex, cursor.spelling):
                    returned_regex = ".^"  # Regex to match nothing

    return returned_regex


def regex_generator_general_rule_5(cursor):
    """ 
    Regex pattern that matches either upper-case or lower-case variations of <file name>__<anything>
    """
    expected_prefix_lowercase = os.path.basename(cursor.location.file.name).split(".")[0]
    expected_prefix_uppercase = expected_prefix_lowercase.upper()

    return (
        "^" + expected_prefix_lowercase + "__(.+)$" + "|^" + expected_prefix_uppercase + "__(.+)$"
    )


def regex_generator_variable_rule_1(cursor):
    """ 
    Regex pattern that doesn't match anything, used because any cursor that
    meets the 'cursor_kinds' and 'exceptions' criteria for this rule is
    a non-const static local variable declaration, so the regex pattern that
    the cursor spelling is compared against must always not match
    """

    return ".^"


def regex_generator_macro_rule_1(cursor):
    """ Regex pattern for required header guard name """
    returned_regex = "."  # Regex to match anything

    if re.search("_H$", cursor.spelling):
        upper_case_file_name = os.path.basename(cursor.location.file.name).split(".")[0].upper()
        expected_header_guard_regex = "^" + upper_case_file_name + "_H$"

        if not re.match(expected_header_guard_regex, cursor.spelling):
            returned_regex = ".^"  # Regex to match nothing

    return returned_regex


def regex_generator_struct_rule_1(cursor):
    return "^.*_(tag){1}$"


def regex_generator_struct_rule_2(cursor):
    return "^.*_[s]{1}$"


def regex_generator_struct_rule_3(cursor):
    """ Regex pattern for <module name>__<struct name>_s """
    expected_prefix = os.path.basename(cursor.location.file.name).split(".")[0]

    return "^(({}__).+(_s))|({}_s)$".format(expected_prefix, expected_prefix)


def regex_generator_struct_rule_4(cursor):
    """ Regex pattern to ensure that typedef struct name is in form <struct tag without suffix>_s """
    return generate_expected_typedef_name_regex("_s", cursor)


def regex_generator_union_rule_1(cursor):
    return "^.*_(tag){1}$"


def regex_generator_union_rule_2(cursor):
    return "^.*_[u]{1}$"


def regex_generator_union_rule_3(cursor):
    """ Regex pattern for <module name>__<union name>_u """
    expected_prefix = os.path.basename(cursor.location.file.name).split(".")[0]

    return "^({}__).+(_u)$".format(expected_prefix)


def regex_generator_union_rule_4(cursor):
    """ Regex pattern to ensure that typedef union name is in form <union tag without suffix>_u """
    return generate_expected_typedef_name_regex("_u", cursor)


def regex_generator_enum_rule_1(cursor):
    return "^.*_(tag){1}$"


def regex_generator_enum_rule_2(cursor):
    return "^.*_[e]{1}$"


def regex_generator_enum_rule_3(cursor):
    """ Regex pattern for <module name>__<enum name>_e """
    expected_prefix = os.path.basename(cursor.location.file.name).split(".")[0]

    return "^({}__).+(_e)$".format(expected_prefix)


def regex_generator_enum_rule_4(cursor):
    """ Regex pattern to ensure that typedef enum name is in form <enum tag without suffix>_e """
    return generate_expected_typedef_name_regex("_e", cursor)


def regex_generator_enum_rule_5(cursor):
    """ Regex pattern for <enum base name>_<purpose> in UPPER_CASE 

    e.g.

                typedef enum enum_tag {
                    enum_member,
                } typedef_enum_name

    Example enum above has AST in format below. Algorithm below finds 
    typedef_enum_name for enum_member.

                  grandparent_cursor
                    |             |
                    v             v
              enum_tag   <---   typedef_enum_name
                    |
                    v
                enum_member (cursor argument)
    """
    parent_cursor = cursor.lexical_parent
    grandparent_cursor = cursor.lexical_parent.lexical_parent

    typedef_enum_cursor = None  # Cursor for typedef_enum_name in diagram above

    for uncle_cursor in grandparent_cursor.get_children():
        for cousin_cursor in uncle_cursor.get_children():
            if cousin_cursor == parent_cursor:
                typedef_enum_cursor = uncle_cursor

    expected_enum_member_regex = "."  # Regex pattern to match anything

    if typedef_enum_cursor is None:
        pass  # Enum is anonymous enum so members don't require prefix
    else:
        typedef_enum_name = typedef_enum_cursor.spelling
        if typedef_enum_name[len("_e") * -1 :] == "_e":
            expected_enum_member_regex = (
                "^(" + typedef_enum_name[: len("_e") * -1].upper() + "_)(.+)$"
            )
        else:  # If typedef enum name is not valid, it will be caught by another rule, so is ignored by this rule
            pass

    return expected_enum_member_regex


def regex_generator_function_rule_1(cursor):
    """ Regex pattern for <module name>__<function name>() """
    file_name = os.path.basename(cursor.location.file.name).split(".")[0]

    return "^({}__).+$".format(file_name)


def regex_generator_function_rule_2(cursor):
    """ Regex pattern for <module name>__private_<function name>() """
    raw_file_name = os.path.basename(cursor.location.file.name).split(".")[0]

    file_name = re.sub("(_private)$", "", raw_file_name)

    return "^({}__private_).+$".format(file_name)


def regex_generator_function_rule_3(cursor):
    return "^.*_[f]{1}$"


regex_dict = {
    "general_rule_1": regex_generator_general_rule_1,
    "general_rule_2": regex_generator_general_rule_2,
    "general_rule_3": regex_generator_general_rule_3,
    "general_rule_4": regex_generator_general_rule_4,
    "general_rule_5": regex_generator_general_rule_5,
    "variable_rule_1": regex_generator_variable_rule_1,
    "macro_rule_1": regex_generator_macro_rule_1,
    "struct_rule_1": regex_generator_struct_rule_1,
    "struct_rule_2": regex_generator_struct_rule_2,
    "struct_rule_3": regex_generator_struct_rule_3,
    "struct_rule_4": regex_generator_struct_rule_4,
    "union_rule_1": regex_generator_union_rule_1,
    "union_rule_2": regex_generator_union_rule_2,
    "union_rule_3": regex_generator_union_rule_3,
    "union_rule_4": regex_generator_union_rule_4,
    "enum_rule_1": regex_generator_enum_rule_1,
    "enum_rule_2": regex_generator_enum_rule_2,
    "enum_rule_3": regex_generator_enum_rule_3,
    "enum_rule_4": regex_generator_enum_rule_4,
    "enum_rule_5": regex_generator_enum_rule_5,
    "function_rule_1": regex_generator_function_rule_1,
    "function_rule_2": regex_generator_function_rule_2,
    "function_rule_3": regex_generator_function_rule_3,
}


def generate_expected_typedef_name_regex(aggregate_type_suffix, cursor):
    """ This helper function returns the expected struct/union/enum typedef name for a given \
    typedef struct/union/enum cursor and its expected suffix
    """

    assert aggregate_type_suffix in ["_s", "_u", "_e"]

    cursor_children = list(cursor.get_children())
    assert 1 == len(cursor_children)
    cursor_child = cursor_children[0]

    struct_tag = cursor_child.spelling
    expected_typedef_struct_name_regex = "."
    if struct_tag != "":  # If typedef struct name has a struct tag
        if "_tag" in struct_tag:
            expected_typedef_struct_name_regex = (
                "^" + struct_tag[: len("_tag") * -1] + aggregate_type_suffix + "$"
            )
        else:
            pass  # If the struct tag name does not end in the correct suffix, will fail other rules, so returned regex matches everything
    else:
        pass  # No struct tag name exists so returned regex matches everything

    return expected_typedef_struct_name_regex
