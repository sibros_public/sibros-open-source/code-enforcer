import os
import platform
import subprocess
import sys

SUPPORTED_LIBCLANG_VERSIONS = [11, 10, 9, 8, 7, 6]


def get_libclang_path_by_os():
    """
    Locates a user's libclang.so library.

    Search Strategy:
    1. If a Conda environment is detected by the presense of $CONDA_PREFIX, it will be searched first
    2. If no libclang.so has been found yet, and a libclang can be found through 'which', it will be chosen
        * Systems with multiple libclang installations may version them as libclang.so (default), libclang-8.so, etc.
        * In these cases the system default takes priority, followed by versioned libclang installataions,
            with priority given to the most recent libclang installation.
    3. As a last resort, some hard coded paths may be assumed
    """
    # Libclang may be stored as libclang-N, search for and support any of these versions in order of newest to oldest

    libclang_path = None

    os_type = platform.system()
    if "Linux" or "Darwin" in os_type:
        # 1. Conda installation
        conda_env_location = os.environ.get("CONDA_PREFIX")
        if conda_env_location:
            search_directory = os.path.join(conda_env_location, "lib")
            for filename in os.listdir(search_directory):
                if filename == ("libclang.so"):
                    libclang_path = os.path.join(
                        search_directory, filename
                    )  # Required to run on Mac
                elif filename == ("libclang.dylib"):
                    libclang_path = os.path.join(search_directory, filename)
        # 2. locate through system 'which'
        if not libclang_path:
            # Example output: 'libclang: /usr/lib/libclang.so /usr/local/lib/libclang.so'
            check_paths = (
                str(subprocess.check_output(["whereis", "libclang"]).decode(sys.stdout.encoding))
                .strip()
                .split()[1:]
            )
            # Check for versioned 'libclang' as well with lower priority
            for version in SUPPORTED_LIBCLANG_VERSIONS:
                check_paths += (
                    str(
                        subprocess.check_output(["whereis", "libclang-{}".format(version)]).decode(
                            sys.stdout.encoding
                        )
                    )
                    .strip()
                    .split()[1:]
                )
            for path in check_paths:
                if ".so" in path:
                    libclang_path = path
                    break

    # 3. Hardcoded path
    if not libclang_path:
        if "Darwin" in os_type:
            path_assumption = "/usr/local/opt/llvm@{}/lib/libclang.dylib"
            for version in SUPPORTED_LIBCLANG_VERSIONS:
                libclang_guess = path_assumption.format(version)
                if os.path.exists(libclang_guess):
                    libclang_path = libclang_guess
                    break
        elif "Windows" in os_type:
            libclang_guess = "C:/Program Files/LLVM/bin/libclang.dll"
            if os.path.exists(libclang_guess):
                libclang_path = libclang_guess
    return libclang_path
