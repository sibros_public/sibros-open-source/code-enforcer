import yaml
import re
import sys


class IgnorefileParser:
    def __init__(self, ignorefile_path=None):
        if ignorefile_path == None:
            raise FileNotFoundError

        self._ignorefile_path = ignorefile_path

        with open(self._ignorefile_path) as file:
            data_dict = yaml.load(file, Loader=yaml.FullLoader)
            self._cursor_exception_list = data_dict.get("cursor_exceptions")
            self._file_rule_regex_exception_list = data_dict.get("file_rule_regex_exceptions")
            self._file_regex_exception_list = data_dict.get("file_regex_exceptions")

        assert self._is_ignorefile_valid()

    #
    # Public methods
    #

    def is_file_ignored(self, filepath):
        is_ignored = False

        if self._file_regex_exception_list != None:
            for file_regex_exception in self._file_regex_exception_list:
                if re.search(file_regex_exception, filepath):
                    is_ignored = True

        return is_ignored

    def is_cursor_ignored_by_rule(self, cursor, rule):
        if self._cursor_exception_list is None:
            return False

        exceptions_matched_by_file_list = []
        exceptions_matched_by_cursor_list = []
        is_ignored = False

        # Get list of exceptions that match the cursor's file name
        exceptions_matched_by_file_list = list(
            filter(
                lambda exception: exception["file"] in cursor.location.file.name,
                self._cursor_exception_list,
            )
        )

        # Get list of exceptions that ALSO match cursor name
        if len(exceptions_matched_by_file_list) > 0:
            exceptions_matched_by_cursor_list = list(
                filter(
                    lambda exception: cursor.spelling == exception["symbol"],
                    exceptions_matched_by_file_list[0].get("cursors"),
                )
            )

        if len(exceptions_matched_by_cursor_list) > 0:
            if rule.name in exceptions_matched_by_cursor_list[0].get("rules"):
                is_ignored = True

        return is_ignored

    def is_cursor_ignored_by_rule_in_file(self, cursor, rule):
        is_ignored = False

        if self._file_rule_regex_exception_list != None:
            for file_rule_regex_exception in self._file_rule_regex_exception_list:
                if re.search(
                    file_rule_regex_exception.get("file_regex"), cursor.location.file.name
                ):
                    excepted_rules_list = file_rule_regex_exception.get("rules")
                    if rule.name in excepted_rules_list:
                        is_ignored = True

        return is_ignored

    #
    # Private methods
    #

    def _is_ignorefile_valid(self):
        if self._cursor_exception_list is None:
            return False

        # Check for duplicate file entry
        file_list = [exception["file"] for exception in self._cursor_exception_list]
        duplicate_file_list = set(
            [duplicate_file for duplicate_file in file_list if file_list.count(duplicate_file) > 1]
        )

        if len(duplicate_file_list) > 0:
            print(
                "Ignorefile YAML has multiple instances of filepath(s):\n{}".format(
                    "\n".join(duplicate_file_list)
                )
            )
            sys.exit(1)

        # Check for duplicate cursor for each file entry
        for file_entry in self._cursor_exception_list:
            current_file = file_entry["file"]
            cursor_name_list = [cursor["symbol"] for cursor in file_entry["cursors"]]
            duplicate_cursor_name_list = set(
                [
                    duplicate_cursor
                    for duplicate_cursor in cursor_name_list
                    if cursor_name_list.count(duplicate_cursor) > 1
                ]
            )

            if len(duplicate_cursor_name_list) > 0:
                print(
                    "Ignorefile YAML has multiple instances of cursor(s):\n{}\nin file entry <{}>".format(
                        "\n".join(duplicate_cursor_name_list), current_file
                    )
                )
                sys.exit(1)

        # Check for duplicate rules for each cursor for each file entry
        for file_entry in self._cursor_exception_list:
            current_file = file_entry["file"]
            for symbol in file_entry["cursors"]:
                current_symbol = symbol["symbol"]
                rule_list = symbol["rules"]
                duplicate_rule_list = duplicate_file_list = set(
                    [
                        duplicate_rule
                        for duplicate_rule in rule_list
                        if rule_list.count(duplicate_rule) > 1
                    ]
                )

                if len(duplicate_rule_list) > 0:
                    print(
                        "Ignorefile YAML has multiple instances of rules(s):\n{}\nin cursor <{}> in file entry <{}>".format(
                            "\n".join(duplicate_rule_list), current_symbol, current_file
                        )
                    )
                    sys.exit(1)

        return True

    #
    # Accessors
    #

    def get_ignorefile_path(self):
        return self._ignorefile_path

    def get_cursor_exception_list(self):
        return self._cursor_exception_list

    def get_file_rule_regex_exception_list(self):
        return self._file_rule_regex_exception_list

    def get_file_regex_exception_list(self):
        return self._file_regex_exception_list

    #
    # Mutators
    #
