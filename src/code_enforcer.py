import os
import re

from code_parser import CodeParser
from rules import Rule, ViolationError
from violation_summary import ViolationSummary


class CodeEnforcer(CodeParser):
    def __init__(
        self,
        rules,
        verbose=False,
        ignorefile_parser=None,
        excepted_file_list=None,
        filepath=None,
        compile_commands_dir=None,
        parse_args=None,
        libclang_path=None,
    ):
        """
        :param rules: A list of rules to enforce (list of Rule)
        :param kwargs:
        """
        CodeParser.__init__(
            self,
            filepath=filepath,
            compile_commands_dir=compile_commands_dir,
            parse_args=parse_args,
            libclang_path=libclang_path,
        )

        assert isinstance(rules, list)
        for rule in rules:
            assert isinstance(rule, Rule)
        self._rules = rules

        self._verbose = verbose
        self._violation_summary = ViolationSummary()

        self._ignorefile_parser = ignorefile_parser
        self._excepted_file_list = excepted_file_list

    """
    Public methods
    """

    def reset(self, **kwargs):
        CodeParser.reset(self, **kwargs)
        self._violation_summary = ViolationSummary()

    def run(self):
        """
        Perform code enforcement
        :return: True if file conforms to the list of rules
        """

        success = True
        for cursor in self.parse():
            if not self._filter_by_cursor(cursor):
                continue

            if self._verbose:
                print(
                    "{} {} {}:{}:{}".format(
                        cursor.spelling,
                        cursor.kind,
                        os.path.basename(cursor.location.file.name),
                        cursor.location.line,
                        cursor.location.column,
                    )
                )

            for rule in self._rules:
                if self._filter_by_rule(cursor, rule):
                    try:
                        rule.enforce(cursor)
                    except ViolationError:
                        self._violation_summary.add_violation(cursor=cursor, rule=rule)
                        success = False

        return success

    """
    Private methods
    """

    def _is_cursor_excepted_by_tag(self, cursor):
        if self._excepted_file_list is None:
            return False

        is_excepted = False

        if cursor.location.file.name in self._excepted_file_list:
            is_excepted = True

        return is_excepted

    def _filter_by_cursor(self, cursor):
        """
        Perform filter to ensure cursor should be analyzed; all other cursors should be discarded
        :param cursor: Clang cursor (Cursor)
        :return: True if cursor should be analyzed
        """
        valid = True

        if cursor.location.file is None:
            valid = False
        elif self._ignorefile_parser.is_file_ignored(cursor.location.file.name):
            valid = False
        elif self._is_cursor_excepted_by_tag(cursor):
            valid = False
        elif cursor.spelling.strip() == "":
            valid = False
        else:
            pass

        return valid

    def _filter_by_rule(self, cursor, rule):
        """
        Perform filter to ensure cursor should be analyzed based on rule applied to it
        :param cursor: Clang cursor (Cursor)
        :param rule: Rule that is being applied to the cursor
        :return: True if cursor should be analyzed
        """
        valid = True

        if self._ignorefile_parser.is_cursor_ignored_by_rule(cursor, rule):
            valid = False
        elif self._ignorefile_parser.is_cursor_ignored_by_rule_in_file(cursor, rule):
            valid = False
        else:
            pass

        return valid

    """
    Accessors
    """

    @property
    def violation_summary(self):
        return str(self._violation_summary)

    @property
    def violation_brief(self):
        briefs = ""
        for brief in self._violation_summary.iter_brief():
            briefs += "{}\n\n".format(brief)
        return briefs
