from collections import namedtuple
import inspect
import os

from clang.cindex import Cursor
from prettytable import PrettyTable

from rules import Rule

Violation = namedtuple("Violation", ["cursor", "rule"])

LONG_LINE = "======================================================================================================\n"


class ViolationSummary(object):
    def __init__(self):
        self._violations = []  # list of Violation

    def __iter__(self):
        for cursor, rule in self._violations:
            yield cursor, rule

    def __str__(self):
        columns = ["Violation", "Location"] + Rule.COLUMNS
        pretty_table = PrettyTable(field_names=columns)

        for field_name in pretty_table.field_names:
            pretty_table.align[field_name] = "l"

        for cursor, rule in self._violations:
            row = [
                cursor.spelling,
                "{}:{}:{}".format(
                    os.path.basename(cursor.location.file.name),
                    cursor.location.line,
                    cursor.location.column,
                ),
            ] + list(rule)
            pretty_table.add_row(row)

        return pretty_table.get_string()

    """
    Public methods
    """

    def add_violation(self, cursor, rule):
        assert isinstance(cursor, Cursor)
        assert isinstance(rule, Rule)
        violation = Violation(cursor, rule)
        self._violations.append(violation)

    def iter_brief(self):
        for cursor, rule in self:
            yield self._generate_brief_violation_message(cursor, rule)

    """
    Private method
    """

    @staticmethod
    def _generate_brief_violation_message(cursor, rule):
        message = inspect.cleandoc(
            LONG_LINE
            + "Cursor:\t\t[{}]\nLocation:\t{}:{}:{}\nRule:\t\t[{}] {}\nDescription:\n {}".format(
                cursor.spelling,
                cursor.location.file.name,
                cursor.location.line,
                cursor.location.column,
                rule.name,
                rule.brief,
                rule.description,
            )
        )
        return message
