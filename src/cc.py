#!/usr/bin/env python3

import sys
import yaml

import argparse
from multiprocessing import Pool, cpu_count
from functools import partial
import os
import platform
import subprocess
import json
import re

from code_enforcer import CodeEnforcer
import sibros_rules
from libclang_utilities import get_libclang_path_by_os
from ignorefile_parser import IgnorefileParser


SELF_DIRPATH = os.path.dirname(__file__)
DEFAULT_SUMMARY_DEST_DIRPATH = os.path.join(SELF_DIRPATH, "summary")


# argv is added for test_cc purposes
def get_args(argv=None):
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument(
        "-c",
        dest="filepaths",  # List of sources to be enforced
        metavar="<C/C++ files>",
        nargs="*",
        required=True,
    )
    arg_parser.add_argument(
        "-o",
        dest="output_summary",  # Optional user specified output file for enforcement results
        metavar="<summary output filepath>",
        default=DEFAULT_SUMMARY_DEST_DIRPATH,
        required=True,
    )
    arg_parser.add_argument(
        "--code-enforcer-verbose", dest="code_enforcer_verbose", action="store_true"
    )
    arg_parser.add_argument(
        "--ignorefile", dest="ignorefile_path", metavar="<yaml ignorefile path>", default=None
    )
    arg_parser.add_argument(
        "--excepted-files",
        dest="excepted_file_list",
        metavar="<excepted files list>",
        nargs="*",
        default=None,
    )
    arg_parser.add_argument(
        "--libclang-path", dest="libclang_path", metavar="<libclang path>", default=None
    )
    arg_parser.add_argument(
        "--code-enforcer-num-threads",
        dest="code_enforcer_num_threads",
        metavar="<number of threads used to run code enforcer>",
        default=1,
    )

    return arg_parser.parse_known_args(argv)


def run_code_enforcer(
    gcc_args, libclang_path, output_path, verbose, ignorefile_parser, excepted_file_list, filepath
):
    success = False

    if not ignorefile_parser.is_file_ignored(filepath):
        try:
            code_enforcer = CodeEnforcer(
                filepath=filepath,
                parse_args=gcc_args,
                rules=sibros_rules.all_rules,
                libclang_path=libclang_path,
                verbose=verbose,
                ignorefile_parser=ignorefile_parser,
                excepted_file_list=excepted_file_list,
            )
            success = code_enforcer.run()

            if success:
                message_string = "No code enforcer violations"
                print(message_string)
            else:
                message_string = code_enforcer.violation_summary
                print(code_enforcer.violation_brief)

        except RuntimeError as error:
            message_string = "Error enforcing file\n"
            print(error)

    else:
        message_string = "Ignoring file {}\n".format(filepath)
        success = True

    with open(output_path, "w") as file_obj:
        file_obj.write(message_string)

    return success


def main():
    (args, gcc_args) = get_args()
    filepaths = args.filepaths
    output_summary = args.output_summary
    code_enforcer_verbose = args.code_enforcer_verbose
    ignorefile_path = args.ignorefile_path
    excepted_file_list = args.excepted_file_list
    libclang_path = args.libclang_path
    num_threads = int(args.code_enforcer_num_threads)

    if libclang_path is None:
        libclang_path = get_libclang_path_by_os()
        if libclang_path is None:
            print("libclang path could not be found")
            raise FileNotFoundError

    ignorefile_parser = IgnorefileParser(ignorefile_path=ignorefile_path)

    num_threads = 1 if num_threads < 1 else num_threads

    if (gcc_args != None) and (filepaths != None):
        gcc_args_str = " ".join(gcc_args)

        if num_threads == 1:
            code_enforcer_success = True

            for filepath in filepaths:
                if not run_code_enforcer(
                    gcc_args_str,
                    libclang_path,
                    output_summary,
                    code_enforcer_verbose,
                    ignorefile_parser,
                    excepted_file_list,
                    filepath,
                ):
                    code_enforcer_success = False

            error_code = 0 if code_enforcer_success else 1
        else:  # See above comment
            with Pool(processes=num_threads) as pool:
                return_values = pool.map(
                    partial(
                        run_code_enforcer,
                        gcc_args_str,
                        libclang_path,
                        output_summary,
                        code_enforcer_verbose,
                        ignorefile_parser,
                        excepted_file_list,
                    ),
                    filepaths,
                )
                code_enforcer_success = all(return_values)

            error_code = 0 if code_enforcer_success else 1
    else:
        print("Incorrect parameters, exiting")
        sys.exit(1)

    if error_code != 0:
        for filepath in filepaths:
            print("Code enforcer ran with errors for files: <{}>\n\n".format(filepath))

    return error_code


if __name__ == "__main__":
    sys.exit(main())
