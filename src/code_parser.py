import json
import os
import subprocess
import sys

from clang.cindex import Index, Config, CompilationDatabase, TranslationUnit


class CodeParser(object):
    def __init__(self, filepath, compile_commands_dir=None, parse_args=None, libclang_path=None):
        """
        :param filepath: File path of source/header file to be parsed (str)
        :param compile_commands_dir: File path of compile commands .json to be parsed (str)
        :param parse_args: A list of compiler parser arguments (list of str)
        """
        if (libclang_path is not None) and (Config.loaded is False):
            Config.set_library_file(libclang_path)

        CodeParser.reset(self, filepath, compile_commands_dir, parse_args)

    """
    Public methods
    """

    def reset(self, filepath, compile_commands_dir=None, parse_args=None):
        assert os.path.isfile(filepath)
        self._filepath = filepath

        if isinstance(parse_args, str):
            parse_args = parse_args.split(" ")
        else:
            parse_args = []
            parse_args.extend(self._get_compile_commands(compile_commands_dir))

        self._parse_args = parse_args

    def parse(self):
        index = Index.create()
        translation_unit = index.parse(
            self._filepath,
            args=self._parse_args,
            options=TranslationUnit.PARSE_DETAILED_PROCESSING_RECORD,
        )
        if translation_unit.diagnostics:
            for diag in translation_unit.diagnostics:
                print(diag, file=sys.stderr)
            raise RuntimeError("Libclang failed to parse file")
        for cursor in self._iter_translation_unit(translation_unit.cursor):
            yield cursor

    """
    Private methods
    """

    def _iter_translation_unit(self, cursor):
        # for first call, returns first Abstract syntax tree node of parsed file
        yield cursor
        for cursor_to_child in cursor.get_children():
            yield from self._iter_translation_unit(cursor_to_child)

    def _get_compile_commands(self, compile_commands_dir):
        compdb = CompilationDatabase.fromDirectory(compile_commands_dir)
        file_args = compdb.getCompileCommands(self._filepath)
        args_list = [arg for arg in file_args[0].arguments]
        return args_list
