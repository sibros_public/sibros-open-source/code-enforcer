from clang.cindex import StorageClass, CursorKind, LinkageKind
import re
import os


#
# Sibros Exceptions
#


def except_static_const_global_variable(cursor):
    """ Returns true if the cursor is a static const global variable """
    return_code = False

    if cursor.storage_class == StorageClass.STATIC and is_cursor_const(
        cursor
    ):  # If cursor is static const
        if is_cursor_global_scope(cursor):  # If cursor is global scope
            return_code = True

    return return_code


def except_non_const_variable(cursor):
    """ Returns true if the cursor is a variable that is not const"""
    return_code = False

    if cursor.kind == CursorKind.VAR_DECL and not is_cursor_const(cursor):
        return_code = True

    return return_code


def except_global_variable(cursor):
    """ Returns true if the cursor is a global variable """
    return is_cursor_global_scope(cursor) and cursor.kind == CursorKind.VAR_DECL


def except_const_variable(cursor):
    """ Returns true if the cursor is a const variable """

    return is_cursor_const(cursor)


def except_non_static_variable(cursor):
    """ Returns true if the cursor is a non-static variable """
    return not cursor.storage_class == StorageClass.STATIC and cursor.kind == CursorKind.VAR_DECL


def except_extern(cursor):
    """ Returns true if the cursor is an extern variable or function """
    return_code = False

    if cursor.kind != CursorKind.MACRO_DEFINITION:
        return_code = cursor.storage_class == StorageClass.EXTERN

    return return_code


def except_const_aggregate_types(cursor):
    """ Returns true if the cursor is a const global aggregate type or a const global who's underlying typedef is an aggregate type """

    return_code = False

    if is_cursor_const(cursor) and cursor.kind == CursorKind.VAR_DECL:
        return_code = is_cursor_aggregate_type(cursor)

    return return_code


def except_non_static_non_const_local_variable(cursor):
    """ Returns true if the cursor is a local variable that is not static const"""
    return_code = False

    if cursor.kind == CursorKind.VAR_DECL and not os.path.isfile(
        cursor.lexical_parent.spelling
    ):  # If cursor is not global and is a variable
        if not cursor.storage_class == StorageClass.STATIC or not is_cursor_const(
            cursor
        ):  # If cursor is not static or not const
            return_code = True

    return return_code


def except_well_known_macros(cursor):
    """ Returns true if the cursor is both a variable declaration, and matches a regex pattern corresponding to well known macros. """
    WELL_KNOWN_MACROS_REGEX = "(COMPILE_TIME_ASSERT_.*)"

    cursor_is_a_well_known_macro = False
    if cursor.kind in [CursorKind.VAR_DECL, CursorKind.MACRO_DEFINITION]:
        if re.search(WELL_KNOWN_MACROS_REGEX, cursor.spelling):
            cursor_is_a_well_known_macro = True

    return cursor_is_a_well_known_macro


def except_non_struct_typedefs(cursor):
    """ Returns true if the cursor is a typedef declaration and who's underlying typedef spelling does not have 'struct' in it """
    return_value = True
    if CursorKind.TYPEDEF_DECL == cursor.kind:
        if re.match("^(struct ).+$", cursor.underlying_typedef_type.spelling):
            """ Matches cursor's underlying typedef spelling if it has format "struct <any name>" """
            return_value = False

    return return_value


def except_non_union_typedefs(cursor):
    """ Returns true if the cursor is a typedef declaration and who's underlying typedef spelling does not have 'union' in it """
    return_value = True
    if CursorKind.TYPEDEF_DECL == cursor.kind:
        if re.match("^(union ).+$", cursor.underlying_typedef_type.spelling):
            """ Matches cursor's underlying typedef spelling if it has format "union <any name>" """
            return_value = False

    return return_value


def except_non_enum_typedefs(cursor):
    """ Returns true if the cursor is a typedef declaration and who's underlying typedef spelling does not have 'union' in it """
    return_value = True
    if CursorKind.TYPEDEF_DECL == cursor.kind:
        if re.match("^(enum ).+$", cursor.underlying_typedef_type.spelling):
            """ Matches cursor's underlying typedef spelling if it has format "enum <any name>" """
            return_value = False

    return return_value


def except_non_function_pointer_typedefs(cursor):
    """ Returns true if the cursor is a typedef declaration and who's underlying typedef spelling does not match a function pointer regex """
    return_value = True
    if CursorKind.TYPEDEF_DECL == cursor.kind:
        if re.match(r".*( \(\*\))\(.*\)$", cursor.underlying_typedef_type.spelling):
            """ Matches cursor's underlying typedef spelling if it has format "<return type> (*)(<param 1>)(...)" """
            return_value = False

    return return_value


def except_non_general_typedefs(cursor):
    """ Returns true if the cursor is a typedef declaration and who's underlying typedef isn't struct, union, enum, or function pointer """
    return_value = True

    if (
        except_non_struct_typedefs(cursor)
        and except_non_union_typedefs(cursor)
        and except_non_enum_typedefs(cursor)
        and except_non_function_pointer_typedefs(cursor)
    ):
        return_value = False

    return return_value


def except_non_private_functions(cursor):
    """ Returns true if the cursor is a non-private function """
    return cursor.kind == CursorKind.FUNCTION_DECL and cursor.linkage == LinkageKind.EXTERNAL


def except_private_functions(cursor):
    """ Returns true if the cursor is a private function """
    return cursor.kind == CursorKind.FUNCTION_DECL and cursor.linkage == LinkageKind.INTERNAL


def except_local_cursor(cursor):
    """ Returns true if the cursor is local scope """
    return not is_cursor_global_scope(cursor)


def except_header_guard_macro(cursor):
    """ Returns true if the cursor is a macro in the format SIBROS__<name>_H """
    return cursor.kind == CursorKind.MACRO_DEFINITION and re.match(
        "^SIBROS__(?:[A-Z0-9]+_+)*[A-Z0-9]+_H$", cursor.spelling
    )


#
# Helper Functions
#


def is_function_non_static(cursor):
    """ Returns true if the cursor is a non-static function declaration """
    return_value = True
    if CursorKind.FUNCTION_DECL == cursor.kind:
        if StorageClass.STATIC == cursor.storage_class:
            return_value = False

    return return_value


def is_function_static(cursor):
    """ Returns true if the cursor is a static function declaration """
    return_value = False
    if CursorKind.FUNCTION_DECL == cursor.kind:
        if StorageClass.STATIC == cursor.storage_class:
            return_value = True

    return return_value


def is_function_main(cursor):
    """ Returns true if the cursor is a main function """
    return_value = False
    if CursorKind.FUNCTION_DECL == cursor.kind:
        if cursor.spelling == "main":
            return_value = True

    return return_value


def is_cursor_global_scope(cursor):
    if cursor.kind == CursorKind.MACRO_DEFINITION:
        return True

    return os.path.isfile(cursor.lexical_parent.spelling)


def is_cursor_const(cursor):
    """ Helper function that checks if cursor is const, works for non-array and array types """
    is_cursor_const = False

    if cursor.kind == CursorKind.VAR_DECL:
        try:  # If cursor is an array definition, needs to have its element types checked
            is_cursor_const = cursor.type.element_type.is_const_qualified()
        except:
            is_cursor_const = cursor.type.is_const_qualified()

    return is_cursor_const


def is_cursor_aggregate_type(cursor):
    """ 
    Helper function that checks if cursor or underlying typedef of cursor is aggregate type (struct/union/array) 
    
    Declarations of following are aggregate types
        struct/union
        typedef struct/union
        array of any type
    """

    is_aggregate = False

    try:  # Check if cursor is array
        if cursor.type.element_type is not None:
            is_aggregate = True
    except:
        pass

    aggregate_types = ["struct", "union"]
    for aggregate_type in aggregate_types:  # Check if cursor is struct/union type
        if aggregate_type in cursor.type.spelling:
            is_aggregate = True
            break

    try:  # Check if cursor is typedef of struct/union type
        current_type = cursor.type.get_declaration().underlying_typedef_type
        for aggregate_type in aggregate_types:
            if aggregate_type in current_type.spelling:
                is_aggregate = True
                break
    except:
        pass

    return is_aggregate
