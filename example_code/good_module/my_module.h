#ifndef MY_MODULE_H
#define MY_MODULE_H

/***********************************************************************************************************************
 *
 *                                                   D E F I N E S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                                  T Y P E D E F S
 *
 **********************************************************************************************************************/

typedef struct my_module__my_struct_tag {
  int my_member;
} my_module__my_struct_s;

typedef union my_module__my_union_tag {
  int my_member_one;
  char my_member_two;
} my_module__my_union_u;

typedef enum my_module__my_enum_tag {
  MY_MODULE__MY_ENUM_TAG_DESCRIPTION_ONE = 0,
  MY_MODULE__MY_ENUM_TAG_ANOTHER_DESCRIPTION,
  MY_MODULE__MY_ENUM_TAG_YET_ANOTHER_DESCRIPTION,
} my_module__my_enum_e;

/***********************************************************************************************************************
 *
 *                                     F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/

static void my_module__private_my_function_name(void);

void my_module__my_function_name(void);

#endif /* #ifdef MY_MODULE_H */
