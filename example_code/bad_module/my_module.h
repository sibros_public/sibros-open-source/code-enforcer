#ifndef BAD_NAME_H  // Violates macro_rule_1, not in format <FILE NAME>_H
#define BAD_NAME_H

/***********************************************************************************************************************
 *
 *                                                   D E F I N E S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                                  T Y P E D E F S
 *
 **********************************************************************************************************************/

/* Structs */

typedef struct my_module__my_struct {  // Violates struct_rule_1, not suffixed with _tag
  int my_member;
} my_module__my_struct;  // Violates struct_rule_2, not suffixed with _s

typedef struct my_struct_tag {  // Violates struct_rule_3, not prefixed with file name
  int my_member;
} my_struct_s;  // Violates struct_rule_3, not prefixed with file name

typedef struct my_module__my_struct_tag {
  int my_member;
} my_module__my_other_struct_s;  // Violates struct_rule_4, struct names don't match

/* Unions */

typedef union my_module__my_union {  // Violates union_rule_1, not suffixed with _tag
  int my_member_one;
  char my_member_two;
} my_module__my_union;  // Violates union_rule_2, not suffixed with _u

typedef union my_union_tag {  // Violates union_rule_3, not prefixed with file name
  int my_member_one;
  char my_member_two;
} my_union_u;  // Violates union_rule_3, not prefixed with file name

typedef union my_module__my_union_tag {
  int my_member_one;
  char my_member_two;
} my_module__my_other_union_s;  // Violates union_rule_4, struct names don't match

/* Enums */

typedef enum my_module__my_enum {  // Violates enum_rule_1, not suffixed with _tag
  MY_MODULE__MY_ENUM_TAG_DESCRIPTION_ONE = 0,
  MY_MODULE__MY_ENUM_TAG_DESCRIPTION_TWO,
  MY_MODULE__MY_ENUM_TAG_DESCRIPTION_THREE,
} my_module__my_enum;  // Violates enum_rule_2, not suffixed with _e

typedef enum my_enum_tag {  // Violates enum_rule_3, not prefixed with file name
  MY_ENUM_TAG_DESCRIPTION_FOUR = 0,
  MY_ENUM_TAG_DESCRIPTION_FIVE,
  MY_ENUM_TAG_DESCRIPTION_SIX,
} my_enum_e;  // Violates enum_rule_3, not prefixed with file name

typedef enum my_module__my_enum_tag {
  MY_MODULE__MY_OTHER_ENUM_TAG_DESCRIPTION_SEVEN = 0,
  MY_MODULE__MY_OTHER_ENUM_TAG_DESCRIPTION_EIGHT,
  MY_MODULE__MY_OTHER_ENUM_TAG_DESCRIPTION_NINE,
} my_module__my_other_enum_e;  // Violates enum_rule_4, struct names don't match

typedef enum my_module__my_another_enum_tag {
  MY_MODULE__MY_ANOTHER_BAD_ENUM_TAG_DESCRIPTION_TEN = 0,  // Violates enum_rule_5, enum members don't match enum name
  MY_MODULE__MY_ANOTHER_ENUM_TAG_DESCRIPTION_ELEVEN,
  MY_MODULE__MY_ANOTHER_ENUM_TAG_DESCRIPTION_TWELVE,
} my_module__my_another_enum_e;

/***********************************************************************************************************************
 *
 *                                     F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/

static void my_module__my_function_name(void);  // Violates function_rule_2, __private_ not in name

static void bad__private_my_function_name(void);  // Violates function_rule_2, not prefixed with file name

void my_function_name(void);  // Violates function_rule_1, not prefixed with file name

#endif /* #ifdef BAD_NAME_H */
