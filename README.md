# Code Enforcer

Code Enforcer is a C static analysis tool to enforcer coding standards beyond the capabilities of compilers and linters. 

## Prerequisites

- Python 3.3.x+
- `libclang.so` or `libclang.lib` or `libclang.dylib` shared library
- clang 6.0.0.2
- prettytable 0.7.2

```bash
sudo apt-get install libclang-8-dev
```

```bash
pip3 install -r requirements.txt
```

## Usage

```bash
Usage: 

cc.py -c source_file(s) -o output_summary_path [--code-enforcer-verbose] --ignorefile ignorefile_yaml_path [--excepted-files excepted_files_list] [--libclang-path libclang_library_path] [--code-enforcer-num-threads num_threads] [... additional compile commands]

  -c source_file(s):                        one or more .c files to be enforced
  -o output_summary_path:                   path to write output summary .txt file
  --code-enforcer-verbose:                  verbose terminal output, default is false
  --ignorefile ignorefile_yaml_path         path to code_enforcer_ignore.yaml
  --excepted-files excepted_files_list      excepted .c or .h files, no Code Enforcer rules will be applied to these, default is empty
  --libclang-path libclang_library_path     path to libclang, leave empty to have Code Enforcer attempt to find
  --code-enforcer-num-threads num_threads   number of threads to run Code Enforcer, default is 1
  ... additional compile commands           additional compile commands required for libclang to parse target
```

For example, following commands run Code Enforcer on `example_code/good_module/...`, which is free of violations, and `example_code/bad_module/...`, which contain violations, respectively. These commands write all violation information to an output file `violation_output_good_module.txt` and `violation_output_bad_module.txt`.

```bash
$ python3 src/cc.py -c example_code/good_module/my_module.c -o violation_output_good_module.txt --ignorefile code_enforcer_ignore.yaml

$ python3 src/cc.py -c example_code/bad_module/my_module.c -o violation_output_bad_module.txt --ignorefile code_enforcer_ignore.yaml
```

## Rules

The example rules below are currently implemented.

* **general_rule_1** Lower case snake case naming convention
  * All variable names, function names, struct/union/enum tags, and typedef names shall use `lower_case_snake_case`
    * EXCEPT: Macros
    * EXCEPT: Enumeration values
* **general_rule_2** Upper case snake case naming convention
  * All macros shall use `UPPER_CASE_SNAKE_CASE`
* **general_rule_3** General typedef naming convention
  * Typedef symbols that are not structs/unions/enums/function pointers shall end with the suffix `_t`
* **general_rule_4** Layer access rule
  * Files in certain layers shall not access types/methods/data from restricted layers
* **general_rule_5** Global variable name format
  * Global-scope variables shall be named in the format `<file name>__<purpose>`
* **variable_rule_1** No non-const local static variable declarations
  * Non-const static variables shall not be declared in functions
* **struct_rule_1** Structure tag name suffix
  * Struct tags shall end with the suffix `_tag`
* **struct_rule_2** Typedef structure name suffix
  * Typedef struct names shall end with the suffix `_s`
* **struct_rule_3** Typedef structure name format
  * Typedef struct names shall be named in the format `<file name>__<purpose>_s` or `<file name>_s`
* **struct_rule_4** Typedef struct name and struct tag match 
  * Struct tag name must match typedef struct name
* **union_rule_1** Union tag name suffix
  * Union tags shall end with the suffix `_tag`
* **union_rule_2** Typedef union name suffix
  * Typedef union names shall end with the suffix `_u`
* **union_rule_3** Typedef union name format
  * Typedef union names shall be named in the format `<file name>__<purpose>_u`
* **union_rule_4** Typedef union name and union tag match 
  * Union tag name must match typedef union name
* **enum_rule_1** Enum tag name suffix
  * Enum tags shall end with the suffix `_tag`
* **enum_rule_2** Typedef enum name suffix
  * Typedef enum names shall end with the suffix `_e`
* **enum_rule_3** Typedef enum name format
  * Typedef enum names shall be named in the format `<file name>__<purpose>_e`
* **enum_rule_4** Typedef enum name and enum tag match 
  * Enum tag name must match typedef enum name
* **enum_rule_5** Enum member name format 
  * Enum members shall be named after their enum tag name in the format `<enum base name>_<purpose>` in `UPPER_CASE_SNAKE_CASE`
* **function_rule_1** Non-private function name format
  * Non-static functions shall be named in the format `<file name>__<purpose>`
* **function_rule_2** Private function name format
  * Static functions shall be named in the format `<file name>__private_<purpose>`
* **function_rule_3** Typedef function pointer name suffix
  * Typedef function pointer names shall end with the suffix `_f`

## Exceptions

Exceptions for Code Enforcer can be made for specific symbol names and file regex patterns in `code_enforcer_ignore.yaml`

### Ignorefile Cursor Exceptions

Symbol names can be excepted in code_enforcer_ignore.yaml by adding entries under cursor_exceptions using the following format.

```yaml
cursor_exceptions: 
  - file: <relative filepath from workspace root dir>
    cursors:
      - symbol: <symbol name to be excepted>
        rules:
          - <rule name>
          - <rule name>
      - symbol: <symbol name to be excepted>
        rules:
          - etc.
      - symbol: etc.
  - file: etc.
```

### Ignorefile File Rule Regex Exceptions

All files who’s paths match a specific regex pattern can be excepted in `code_enforcer_ignore.yaml` using the following format.

```yaml
file_rule_regex_exceptions:
  - file_regex: <file path regex pattern>
    rules:
      - <rule name>
      - <rule name>
      - <rule name>
```

### Ignorefile File Regex Exceptions

Entire groups of files can be excepted under file_regex_exceptions: using the following format.

```yaml
file_regex_exceptions:
  - <regex pattern>
  - etc.
```

## Terminal Output
If Code Enforcer detects violations, terminal will output the following format for each violation.

```
======================================================================================================
Cursor:         [MY_LOCAL_VARIABLE]
Location:       example_code/bad_module/my_module.c:73:10
Rule:           [general_rule_1] Lower case snake case naming convention
Description:
 All variable names, function names, struct/union/enum tags, and typedef names shall use lower_case_snake_case, except:
- Global constant non-aggregate types
- Static local constants
- Enumeration values
```

`Cursor` shows the violating symbol name, `Location` shows the file, line, and column of the violating cursor, `Rule` shows the number and brief description of the rule that was violated, and `Description` provides a description of the rule.

If there are compile errors, Code Enforcer will fail and output `Libclang failed to parse file`.

## References
https://github.com/llvm-mirror/clang/tree/master/bindings/python
